# Wordpress-Docker configuration

## Explore container

### Run docker machine env

`$ docker-machine --native-ssh start vm`

`$ docker-machine env vm | Invoke-Expression`

### Launch server cli

List docker containers

`$ docker-compose ps`

Launch cli

`$ docker exec -it <container_name> /bin/bash`